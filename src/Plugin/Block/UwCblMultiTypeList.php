<?php

namespace Drupal\uw_cbl_multi_type_list\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UwCblMultiTypeList block.
 *
 * @Block(
 *   id = "uw_cbl_multi_type_list",
 *   admin_label = @Translation("Multi-type list"),
 * )
 */
class UwCblMultiTypeList extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // If there is config, load in from block config.
    // If there is no config, load in the defaults.
    if (isset($this->configuration['content_to_include'])) {
      $content_to_include = $this->configuration['content_to_include'];
    }
    else {
      $content_to_include = $this->getDefaultConfig();
    }

    // Load in the quicktab object.
    $qt = $this->entityTypeManager->getStorage('quicktabs_instance')->load('uw_qt_multi_type_listing');

    // Get the config data from the quicktab.
    $configuration_data = $qt->getConfigurationData();

    // Step through each of the quicktab config data and check if it should be
    // shown by checking the config values from the block form.
    foreach ($configuration_data as $key => $config_data) {

      // If the content to include is not set, remove it from quicktab config data.
      if (!$content_to_include[$config_data['title']]['shown']) {

        // Unset the quicktab config data.
        unset($configuration_data[$key]);

        // Unset the content to include so that we can use variable later.
        unset($content_to_include[$config_data['title']]);
      }
    }

    // Step through each of the content to include to get things in order.
    foreach ($content_to_include as $key => $include) {

      // Stp through each of the config data and set new config to set in order.
      foreach ($configuration_data as $config_data) {

        // If the key is the same, then place in new array for config.
        if ($key == $config_data['title']) {

          // Set in new config.
          $config_new[] = $config_data;

          // Break out of this loop, since we do not need to continue.
          break;
        }
      }
    }

    // Set the quicktab config data.
    $qt->setConfigurationData($config_new);

    // Return the rendered quicktab.
    return $qt->getRenderArray();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get the content to include from config.
    $content_to_include = $this->configuration['content_to_include'];

    // If content to include is not set, set the items manually.
    // This should only be for new blocks, and have the default settings.
    if ($content_to_include == NULL) {

      // Get the default config.
      $items = $this->getDefaultConfig();
    }
    // If there is content to include from config, set the items
    // to be used in the tables.
    else {

      // Step through each of the content to include and setup the items for the table.
      foreach ($content_to_include as $key => $include) {

        // Each item for the table.
        $items[$key] = [
          'weight' => $include['weight'],
          'shown' => $include['shown'],
        ];
      }
    }

    // The class to be used for groups.
    $group_class = 'group-order-weight';

    // Build the table.
    $form['items'] = [
      '#type' => 'table',
      '#caption' => $this->t('Items'),
      '#header' => [
        $this->t('Listing type'),
        $this->t('Shown'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('No items.'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ]
      ]
    ];

    // Step through each of the items and build the rows.
    foreach ($items as $key => $item) {

      // Make each row draggable.
      $form['items'][$key]['#attributes']['class'][] = 'draggable';

      // Set the weight of the row.
      $form['items'][$key]['#weight'] = $item['weight'];

      // Listing type col.
      $form['items'][$key]['listing_type'] = [
        '#plain_text' => $key,
      ];

      // The shown col.
      $form['items'][$key]['shown'] = [
        '#type' => 'select',
        '#options' => [1 => 'Yes', 0 => 'No'],
        '#default_value' => $item['shown'],
      ];

      // Weight col.
      $form['items'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $item['listing_type']]),
        '#title_display' => 'invisible',
        '#default_value' => $item['weight'],
        '#attributes' => ['class' => [$group_class]],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Set the config for content to include.
    $this->configuration['content_to_include'] = $values['items'];
  }

  /**
   * @return array $items
   *   The default config for this block.
   */
  public function getDefaultConfig() {

    // Blogs.
    $items['Blog'] = [
      'weight' => 1,
      'shown' => 1,
    ];

    // Events.
    $items['Events'] = [
      'weight' => 2,
      'shown' => 1,
    ];

    // News items.
    $items['News'] = [
      'weight' => 3,
      'shown' => 1,
    ];

    return $items;
  }
}
